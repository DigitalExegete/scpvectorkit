//
//  BezierPath+AppKit.swift
//  SCPVectorKit
//
//  Created by Semicolon on 9/27/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import AppKit

extension BezierPathWindingRule
{
    func toNSWindingRule() -> NSWindingRule {
        return NSWindingRule(rawValue: UInt(self.rawValue))!
    }

    static func fromNSWindingRule(windingRule: NSWindingRule) -> BezierPathWindingRule {
        return BezierPathWindingRule(rawValue: Int(windingRule.rawValue))!
    }
}

extension BezierPath  {

    //TODO: rename to var NSBezierPath once we can do so without SourceKit crashing
    public func toNSBezierPath() -> NSBezierPath {
        return NSBezierPath(CGPath: self.CGPath)
    }

    public convenience init(NSBezierPath path: NSBezierPath) {
        self.init(CGPath: path.CGPath)
    }

    public func debugQuickLookObject() -> AnyObject? {
        return self.toNSBezierPath()
    }

    public func simplifiedPathWithThreshold(threshold: CGFloat) -> BezierPath {
        var nsBez = self.toNSBezierPath()
        nsBez = nsBez.fb_fitCurve(threshold)
        return BezierPath(NSBezierPath: nsBez);
    }
}

