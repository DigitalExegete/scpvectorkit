//
//  BezierPath.swift
//  SCPVectorKit
//
//  Created by Semicolon on 9/10/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import Foundation
import CoreGraphics

public enum BezierPathWindingRule: Int {
    case NonZero = 0
    case EvenOdd = 1
}

public enum BezierPathCap: Int {
    case Butt = 0
    case Round = 1
    case Square = 2

    func toCGLineCap() -> CGLineCap {
        switch(self) {
            case .Butt:
                return kCGLineCapButt

            case .Round:
                return kCGLineCapRound

            case .Square:
                return kCGLineCapSquare
        }
    }

    static func fromCGLineCap(CGValue: CGLineCap) -> BezierPathCap? {
        let value = CGValue.value

        if value == kCGLineCapButt.value {
            return .Butt
        }
        else if value == kCGLineCapRound.value {
            return .Round
        }
        else if value == kCGLineCapSquare.value {
            return .Square
        }
        else {
            return nil
        }
    }
}

public enum BezierPathJoin: Int {
    case Miter = 0
    case Round = 1
    case Bevel = 2

    func toCGLineJoin() -> CGLineJoin {
        return CGLineJoin(UInt32(self.rawValue))
    }

    static func fromCGLineJoin(CGValue: CGLineJoin) -> BezierPathJoin? {
        return BezierPathJoin(rawValue: Int(CGValue.value))
    }
}

public func == (lhs: BezierPath, rhs: BezierPath) -> Bool {
    return CGPathEqualToPath(lhs.CGPath, rhs.CGPath) && (lhs.windingRule == rhs.windingRule)
}

@objc (SCPBezierPath) public class BezierPath: NSObject, NSCopying, NSCoding, Equatable {
    /**
        this class is backed by a CGMutablePathRef that does most of the heavy 
        lifting, and is accessible to maintain compatibility with existing 
        CoreGraphics calls and other Apple frameworks. future revisions may use 
        a custom backing store to allow for more customization and easier access
        and mutability than CGPath provides. however, conversions to and from 
        CGPath will still be provided.
    */
    public var CGPath: CGMutablePathRef;

    /**
        windingRule is included as a property because they are required to 
        understand the area that the path represents. different winding rules 
        mean different areas of the path are filled or empty, which affects 
        enough calculations that it should be included with the path object 
        itself. like SVG and AppKit, we default to use the non-zero rule.
    */
    public var windingRule: BezierPathWindingRule = .NonZero

    public var pointsBoundingBox: CGRect {
        return CGPathGetBoundingBox(self.CGPath)
    }

    public var pathBoundingBox: CGRect {
        return CGPathGetPathBoundingBox(self.CGPath)
    }

    public var currentPoint: CGPoint {
        return CGPathGetCurrentPoint(self.CGPath)
    }

    public var isEmpty: Bool {
        return CGPathIsEmpty(self.CGPath)
    }

    public override init() {
        self.CGPath = CGPathCreateMutable()
    }

    public convenience init(CGPath: CGPathRef) {
        self.init()
        CGPathAddPath(self.CGPath, nil, CGPath)
    }

    public convenience init(rect: CGRect) {
        self.init(CGPath: CGPathCreateWithRect(rect, nil))
    }

    public convenience init(roundedRect: CGRect, cornerWidth: CGFloat, cornerHeight: CGFloat) {
        self.init(CGPath: CGPathCreateWithRoundedRect(roundedRect, cornerWidth, cornerHeight, nil));
    }

    public convenience init(ellipseInRect: CGRect) {
        self.init(CGPath: CGPathCreateWithEllipseInRect(ellipseInRect, nil))
    }

    public convenience init(lineFromPoint: CGPoint, toPoint: CGPoint) {
        self.init()
        self.moveToPoint(lineFromPoint)
        self.appendLineToPoint(toPoint)
    }

    //encoding and decoding code adapted from martin winter,
    //https://gist.github.com/9626774.git

    required public init(coder: NSCoder) {
        let data = coder.decodeObjectForKey("CGPathData") as NSData
        self.CGPath = MWCGPathCreateFromNSData(data)
        self.windingRule = BezierPathWindingRule(rawValue: coder.decodeIntegerForKey("windingRule"))!
        super.init()
    }

    public func encodeWithCoder(aCoder: NSCoder) {
        let data = MWNSDataFromCGPath(self.CGPath)
        aCoder.encodeObject(data, forKey:"CGPathData")
        aCoder.encodeInteger(self.windingRule.rawValue, forKey: "windingRule")
    }

    public func copyWithZone(zone: NSZone) -> AnyObject {
        return BezierPath(CGPath: self.CGPath)
    }

    /**
        lots of back-and-forth and consideration on the naming of these methods.
        methods here are given "verb-y" names to reflect that they change the path. hence, append Line and not just line.
        "append" is used instead of "add" to match Swift's use of "append" in arrays
        we're also making sure to follow the "readable as a sentence" approach to methods espoused by Swift,
        with prepositions and first argument names in the method name itself.
        "toPoint" is used for consistency between move, line, and the curve endpoints.
        the curve methods have their control points before the toPoints to match their order in the path itself.
    */

    public func moveToPoint(point: CGPoint) {
        CGPathMoveToPoint(self.CGPath, nil, point.x, point.y)
    }

    public func appendLineToPoint(point: CGPoint) {
        CGPathAddLineToPoint(self.CGPath, nil, point.x, point.y)
    }

    public func appendQuadCurveWithControlPoint(controlPoint: CGPoint, toPoint: CGPoint) {
        CGPathAddQuadCurveToPoint(self.CGPath, nil, controlPoint.x, controlPoint.y, toPoint.x, toPoint.y)
    }

    public func appendCubicCurveWithControlPoint1(controlPoint1: CGPoint, controlPoint2: CGPoint, toPoint: CGPoint) {
        CGPathAddCurveToPoint(self.CGPath, nil, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y, toPoint.x, toPoint.y)
    }

    public func closeSubpath() {
        CGPathCloseSubpath(self.CGPath)
    }

    /**
    while the original CGPath API uses the term "clockwise," we use "reverseDirection" to clarify that
    passing in false draws in a positive direction from startRadians to endRadians. while this leaves
    us with an unfortunate "false means positive" mapping, it maintains exact compatibility with the
    arguments to the CGPath API.
    */

    public func appendArcWithCenter(center: CGPoint, radius: CGFloat, startRadians: CGFloat, endRadians: CGFloat, reverseDirection: Bool) {
        CGPathAddArc(self.CGPath, nil, center.x, center.y, radius, startRadians, endRadians, reverseDirection)
    }

    //TODO: add operator overloads for appending another bezier path
    public func appendPath(bezierPath: BezierPath) {
        CGPathAddPath(self.CGPath, nil, bezierPath.CGPath)
    }

    public func containsPoint(point: CGPoint) -> Bool {
        return CGPathContainsPoint(self.CGPath, nil, point, self.windingRule == .EvenOdd)
    }

    public func pathWithTransform(transform: CGAffineTransform) -> BezierPath {
        var transformPointer = transform
        return BezierPath(CGPath: CGPathCreateCopyByTransformingPath(self.CGPath, &transformPointer))
    }

    public func strokedPathWithWidth(lineWidth: CGFloat, cap: BezierPathCap, join: BezierPathJoin, miterLimit: CGFloat) -> BezierPath {
        let CGCap = cap.toCGLineCap()
        let CGJoin = join.toCGLineJoin()

        let strokedPath = CGPathCreateCopyByStrokingPath(self.CGPath, nil, lineWidth, CGCap, CGJoin, miterLimit)
        return BezierPath(CGPath: strokedPath)
    }

    public func dashedPathWithPhase(phase: CGFloat, lengths: [CGFloat]) -> BezierPath {
        let dashedPath = CGPathCreateCopyByDashingPath(self.CGPath, nil, phase, lengths, UInt(lengths.count))
        return BezierPath(CGPath: dashedPath)
    }
}
