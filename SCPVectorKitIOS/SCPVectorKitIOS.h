//
//  SCPVectorKitIOS.h
//  SCPVectorKitIOS
//
//  Created by Semicolon on 10/23/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SCPVectorKitIOS.
FOUNDATION_EXPORT double SCPVectorKitIOSVersionNumber;

//! Project version string for SCPVectorKitIOS.
FOUNDATION_EXPORT const unsigned char SCPVectorKitIOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SCPVectorKitIOS/PublicHeader.h>

#import "SCPVectorUtilities.h"
#import "FBPathBoolean.h"
#import "MWCGPath.h"
