# SCPVectorKit

SCPVectorKit is a Swift module for OS X and iOS that provides a cross-platform BezierPath class, allowing you to:

- enumerate over the elements of the path
- perform boolean operations (unions, differences, intersects, and exclusive ors)
- use Swift's == operator to compare paths
- encode and decode paths using NSKeyedArchiver and NSKeyedUnarchiver
- apply arbitrary point conversions
- perform curve fitting on flattened paths
- flatten paths with a tolerance

## Implementation Notes

SCPVectorKit's BezierPath class is a lightweight wrapper around CGPath and uses much of CGPath's built-in functionality. Unlike CGPath, SCPVectorKit.BezierPath keeps track of its winding rule, which is used in hit tests and boolean operations. It does not keep track of stroke properties (e.g. line width, cap, join, and dashing), since they are not inherent to the path and its enclosed area.

While NSBezierPath and UIBezierPath provide some of the same functionality as SCPVectorKit.BezierPath, there are differences between their APIs, and they can be unreliable. For example, calling bounds on an empty NSBezierPath will throw an exception, while SCPVectorKit.BezierPath will return the expected CGRectNull.

## Licensing

SCPVectorKit is available under the MIT License, and contains code from other MIT-Licensed sources. See License.txt in the project for more information.

## Acknowledgments

Thanks goes out to:

- Andy Finnell, for his amazing work in [VectorBoolean](https://bitbucket.org/andyfinnell/vectorboolean)
- Martin Winter, for [the NSCoding piece](https://gist.github.com/martinwinter/9626774)
- The [Graphics Gems](http://tog.acm.org/resources/GraphicsGems/) team 
- Mike "Pomax" Kamermans, for the incredible [Beziér primer](http://pomax.github.io/bezierinfo/)

## Contact

Get in touch with the original author at support@semicolonproductions.com or myself at DigitalExegete@gmail.com with questions or suggestions, and get those pull requests submitted. I am more than open to any and all improvements or refinements you may have; let's work together to make this the go-to open source library for vector graphics on Apple's platforms.