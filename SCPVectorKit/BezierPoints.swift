//
//  BezierPoints.swift
//  SCPVectorKit
//
//  Created by Semicolon on 9/29/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import Foundation
import CoreGraphics

//TODO: make this method public and usable in Geometry.swift.
func SCPDotProduct(vector1: CGVector, vector2: CGVector) -> CGFloat {
    return vector1.dx * vector2.dx + vector1.dy * vector2.dy;
}

//TODO: make this method public and usable in Geometry.swift.
func SCPVectorFromPoint(point1: CGPoint, toPoint point2: CGPoint) -> CGVector {
    return CGVector(dx: point2.x - point1.x, dy: point2.y - point1.y)
}

//TODO: make this method public and usable in Geometry.swift.
//thanks to http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
func SCPDistanceBetweenPoint(point: CGPoint, andLineSegmentWithStartPoint startPoint: CGPoint, #endPoint: CGPoint) -> CGFloat {
    let segmentLength = startPoint.distanceToPoint(endPoint);

    //for point with equal starting and end points, just return the distance to segmentP1.
    if segmentLength == 0.0 {
        return point.distanceToPoint(startPoint)
    }

    let dot = SCPDotProduct(SCPVectorFromPoint(startPoint, toPoint: point), SCPVectorFromPoint(startPoint, toPoint:endPoint));
    let t = dot / (segmentLength * segmentLength)

    if t < 0.0 {
        return point.distanceToPoint(startPoint)
    } else if t > 1.0 {
        return point.distanceToPoint(endPoint)
    } else {
        return point.distanceToPoint(BezierPoints.pointAtRatio(t, fromPoint: startPoint, toPoint: endPoint))
    }
}

public struct BezierPoints {
    private var points: [CGPoint]

    public var count: Int {
        return points.count
    }

    public init(points: [CGPoint]) {
        self.points = points
    }

    public subscript(index: Int) -> CGPoint {
        get {
            return points[index]
        }
        set {
            points[index] = newValue
        }
    }

    public var first: CGPoint {
        return points.first!
    }

    public var last: CGPoint {
        return points.last!
    }

    public mutating func applyPointConverter(converter: (CGPoint) -> CGPoint) {
        points = points.map(converter)
    }

    public mutating func evaluateAtRatio(ratio: CGFloat) {
        for index in 0..<self.count - 1 {
            points[index] = BezierPoints.pointAtRatio(ratio, fromPoint: points[index], toPoint: points[index + 1])
        }
        points.removeLast()
    }

    public func pointAtRatio(ratio: CGFloat) -> CGPoint {
        var copied = self
        while copied.count > 1 {
            copied.evaluateAtRatio(ratio)
        }
        return copied.first
    }

    public mutating func convertQuadToCubic() {
        assert(count == 3, "Tried to convert non-quad BezierPoints to cubic")

        let controlPoint1 = BezierPoints.pointAtRatio(2.0 / 3.0, fromPoint: self[0], toPoint: self[1])
        let controlPoint2 = BezierPoints.pointAtRatio(2.0 / 3.0, fromPoint: self[2], toPoint: self[1])

        points = [self[0], controlPoint1, controlPoint2, self[2]]
    }

    public func splitAtRatio(ratio: CGFloat) -> (minPoints: BezierPoints, maxPoints: BezierPoints) {

        var minPoints = self
        var maxPoints = self
        var workingCopy = self

        var minIndex = 1
        var maxIndex = count - 2

        while workingCopy.count > 1 {

            workingCopy.evaluateAtRatio(ratio)
            minPoints[minIndex] = workingCopy[0]
            maxPoints[maxIndex] = workingCopy[maxIndex]

            ++minIndex
            --maxIndex
        }

        return (minPoints, maxPoints)
    }

    //utility geometric function for deCasteljau's algorithm
    public static func pointAtRatio(ratio: CGFloat, fromPoint: CGPoint, toPoint: CGPoint) -> CGPoint {
        let deltaX = ratio * (toPoint.x - fromPoint.x)
        let deltaY = ratio * (toPoint.y - fromPoint.y)
        
        return CGPoint(x: fromPoint.x + deltaX, y: fromPoint.y + deltaY)
    }

    //http://antigrain.com/research/adaptive_bezier/
    public var flatness: CGFloat {
        switch self.count {
        case 2:
            return 0.0

        case 3:
            //using the law of similar triangles, we can "up-convert" the quad's flatness
            //to its equivalent cubic flatness. the cubic flatness is the sum of the
            //distances between the control points and the segment, so 2 * (2/3) * distance.
            return (4.0 / 3.0) * SCPDistanceBetweenPoint(self[1], andLineSegmentWithStartPoint: self[0], endPoint: self[2])

        case 4:
            return SCPDistanceBetweenPoint(self[1], andLineSegmentWithStartPoint: self[0], endPoint: self[3]) +
                SCPDistanceBetweenPoint(self[2], andLineSegmentWithStartPoint: self[0], endPoint: self[3])

        default:
            fatalError("flatness called on BezierPoints of invalid count \(count)")
        }
    }

    public func approximatelyEquals(otherPoints: BezierPoints) -> Bool {

        if count != otherPoints.count {
            return false
        }

        for i in 0..<count {
            let thisPoint = self[i]
            let thatPoint = otherPoints[i]
            if hypot(thisPoint.x - thatPoint.x, thisPoint.y - thatPoint.y) > 0.001 {
                return false
            }
        }

        return true
    }
}
