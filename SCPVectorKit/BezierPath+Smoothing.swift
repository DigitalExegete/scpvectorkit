//
//  BezierPath+Smoothing.swift
//  SCPVectorKit
//
//  Created by Semicolon on 10/24/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import CoreGraphics
import Foundation

extension BezierPath {
    public func smoothedPathWithTolerance(tolerance: CGFloat) -> BezierPath {
        let fitter = BezierPathCurveFitter()
        return fitter.fitCurve(self, errorThreshold: tolerance)
    }
}

private func FBAddPoint(point1: CGPoint, point2: CGPoint) -> CGPoint
{
    return CGPoint(x: point1.x + point2.x, y: point1.y + point2.y)
}

private func FBScalePoint(point: CGPoint, scale: CGFloat) -> CGPoint
{
    return CGPoint(x: point.x * scale, y: point.y * scale)
}

private func FBUnitScalePoint(point: CGPoint, scale: CGFloat) -> CGPoint
{
    var result = point
    let length = hypot(point.x, point.y)
    if ( length != 0.0 ) {
        result.x *= scale/length
        result.y *= scale/length
    }
    return result
}
private func FBDotMultiplyPoint(point1: CGPoint, point2: CGPoint) -> CGFloat
{
    return point1.x * point2.x + point1.y * point2.y
}

private func FBSubtractPoint(point1: CGPoint, point2: CGPoint) -> CGPoint
{
    return CGPoint(x: point1.x - point2.x, y: point1.y - point2.y)
}

private func FBPointSquaredLength(point: CGPoint) -> CGFloat
{
    return (point.x * point.x) + (point.y * point.y)
}

let fitCurveMaximumReparameterizes = 4

private func determinant(matrix1: [CGFloat], matrix2: [CGFloat]) -> CGFloat
{
    return matrix1[0] * matrix2[1] - matrix1[1] * matrix2[0]
}

// The following are 3rd degree Bertstein polynomials. They all have
//  the same formula:
//
//  B[i](input) = (3! / ((3-i)! * i!)) * input^i * (1 - input) ^ (3 - i)
//
//  however the functions below have been optimized for their value of i.
private func Bernstein0(input: CGFloat) -> CGFloat
{
    return CGFloat(powf(1.0 - Float(input), 3))
}

private func Bernstein1(input: CGFloat) -> CGFloat
{
    return CGFloat(3 * Float(input) * powf(1.0 - Float(input), 2))
}

private func Bernstein2(input: CGFloat) -> CGFloat
{
    return CGFloat(3 * powf(Float(input), 2) * (1.0 - Float(input)))
}

private func Bernstein3(input: CGFloat) -> CGFloat
{
    return CGFloat(powf(Float(input), 3))
}

private func NewtonsMethod(#bezierPoints: BezierPoints, #point: CGPoint, #parameter: CGFloat) -> CGFloat
{
    // Use Newton's Method to refine our parameter. In general, that formula is:
    //
    //  parameter = parameter - f(parameter) / f'(parameter)
    //
    // In our case:
    //
    //  f(parameter) = (Q(parameter) - point) * Q'(parameter) = 0
    //
    // Where Q'(parameter) is tangent to the curve at Q(parameter) and orthogonal to [Q(parameter) - P]
    //
    // Taking the derivative gives us:
    //
    //  f'(parameter) = (Q(parameter) - point) * Q''(parameter) + Q'(parameter) * Q'(parameter)
    //

    // We assume the bezier points are a cubic curve

    // Compute Q(parameter)
    var qAtParameter = bezierPoints.pointAtRatio(parameter)

    // Compute Q'(parameter)
    var qPrimePoints = [CGPoint](count: 3, repeatedValue: CGPointZero)
    for i in 0..<3 {
        qPrimePoints[i].x = (bezierPoints[i + 1].x - bezierPoints[i].x) * 3.0;
        qPrimePoints[i].y = (bezierPoints[i + 1].y - bezierPoints[i].y) * 3.0;
    }
    let qPrimeAtParameter = BezierPoints(points: qPrimePoints).pointAtRatio(parameter)

    // Compute Q''(parameter)
    var qPrimePrimePoints = [CGPoint](count: 2, repeatedValue: CGPointZero)
    for i in 0..<2 {
        qPrimePrimePoints[i].x = (qPrimePoints[i + 1].x - qPrimePoints[i].x) * 2.0;
        qPrimePrimePoints[i].y = (qPrimePoints[i + 1].y - qPrimePoints[i].y) * 2.0;
    }
    let qPrimePrimeAtParameter = BezierPoints(points: qPrimePrimePoints).pointAtRatio(parameter);

    // Compute f(parameter) and f'(parameter)
    let qMinusPoint = FBSubtractPoint(qAtParameter, point)
    let fAtParameter = FBDotMultiplyPoint(qMinusPoint, qPrimeAtParameter)
    let fPrimeAtParameter = FBDotMultiplyPoint(qMinusPoint, qPrimePrimeAtParameter) + FBDotMultiplyPoint(qPrimeAtParameter, qPrimeAtParameter)

    // Newton's method!
    return parameter - (fAtParameter / fPrimeAtParameter);
}

struct FitCurveErrorInfo {
    var maximumError: CGFloat
    var maximumIndex: Int
}

// Algorithm implemented here is the one described in "An Algorithm for Automatically Fitting Digitized Curves"
//  by Philip J. Schneider contained in the book Graphics Gems
private class BezierPathCurveFitter: NSObject {
    var points = [CGPoint]()

    func fitCurve(path: BezierPath, errorThreshold: CGFloat) -> BezierPath {
        // Safety first
        if path.isEmpty {
            return BezierPath()
        }

        points = [CGPoint]()

        path.enumerateSegments() { (type: BezierSegmentType, pathPoints: BezierPoints) in
            self.points.append(pathPoints.last)
        }

        let leftT = computeLeftTangentAtIndex(0)
        let rightT = computeRightTangentAtIndex(points.count - 1)

        let result = fitCubicToRange(NSMakeRange(0, points.count), leftTangent:leftT, rightTangent:rightT, errorThreshold:errorThreshold)
        return result
    }

    func computeLeftTangentAtIndex(index: Int) -> CGVector {
        let p1 = points[index + 1], p2 = points[index]
        let dx = p1.x - p2.x, dy = p1.y - p2.y
        let d = hypot(dx, dy)

        return CGVector(dx: dx / d, dy: dy / d)
    }

   func computeRightTangentAtIndex(index: Int) -> CGVector {
        let p1 = points[index - 1], p2 = points[index]
        let dx = p1.x - p2.x, dy = p1.y - p2.y
        let d = hypot(dx, dy)

        return CGVector(dx: dx / d, dy: dy / d)
    }

   func computeCenterTangentAtIndex(index: Int) -> CGVector {

        let p1 = points[index - 1], p2 = points[index], p3 = points[index + 1]
        let dx = ((p1.x - p2.x) + (p2.x - p3.x)) / 2.0
        let dy = ((p1.y - p2.y) + (p2.y - p3.y)) / 2.0
        let d = hypot(dx, dy)

        return CGVector(dx: dx / d, dy: dy / d)
    }

    func fitBezierUsingNaiveMethodInRange(range: NSRange, leftTangent: CGVector, rightTangent: CGVector) -> BezierPoints {
        // This is a fallback method for when our normal bezier curve fitting method fails, either due to too few points
        //  or other anomalies. As with the normal curve fitting, we have the two end points and the direction of the two control
        //  points, meaning we only lack the distance of the control points from their end points. However, instead of getting
        //  all fancy pants in calculating those distances we just throw up our hands and guess that it's a third of the distance between
        //  the two end points. It's a heuristic, and not a good one.

        let startPoint = points[range.location], endPoint = points[range.location + 1]
        let thirdOfDistance = startPoint.distanceToPoint(endPoint) / 3.0

        //TODO: should normalize mutate or return a new vector? should multiplication be an operator overload?
        var leftUnit = leftTangent
        leftUnit.normalize()
        leftUnit.dx *= thirdOfDistance
        leftUnit.dy *= thirdOfDistance
        let cp1 = CGPoint(x: startPoint.x + leftUnit.dx, y: startPoint.y + leftUnit.dy)

        var rightUnit = rightTangent
        rightUnit.normalize()
        rightUnit.dx *= thirdOfDistance
        rightUnit.dy *= thirdOfDistance
        let cp2 = CGPoint(x: endPoint.x + rightUnit.dx, y: endPoint.y + rightUnit.dy)

        return BezierPoints(points: [startPoint, cp1, cp2, endPoint])
    }

    func estimateParametersUsingChordLengthMethodInRange(range: NSRange) -> NSArray
    {
        // We assume that our bezier curve is represented by the function Q(t), where t has the range of [0..1].
        //  The output of Q(t) is (x, y) coordinates that (hopefully) fit closely to our input
        //  points (i.e the points in this bezier path). In this method we're trying to estimate the
        //  parameter of the function Q (i.e. t) for each of our input points. For the end points, this is easy:
        //  Q(0) should give us [self fb_pointAtIndex:range.location] and Q(1) should give us
        //  [self fb_pointAtIndex:range.location + range.length - 1]. For the points in between we'll use the
        //  chord length method.
        //
        // The chord length method assumes a straight line between the points is a reasonable estimate for the curve
        //  between the points. Thus we can estimate t for a given point Foo to be distance in between all the points
        //  from the start point up to the point Foo, divided by the distance between all the points (in order to normalize
        //  the distance between [0..1])

        var distances = [CGFloat](count:range.length, repeatedValue: 0.0)
        distances[0] = 0.0 // First one is always 0 (see above)
        var totalDistance: CGFloat = 0.0
        for i in 1..<range.length {
            // Calculate the total distance along the curve up to this point
            totalDistance += points[range.location + i].distanceToPoint(points[range.location + i - 1])
            distances[i] = totalDistance
        }

        // Now go through and normalize the distances to in the range [0..1]    
        let parameters: [CGFloat] = distances.map({ return $0 / totalDistance })
        return parameters
    }

    func refineParameters(parameters: NSArray, forRange range: NSRange, bezierPoints: BezierPoints) -> NSArray
    {
        // Use Newton's Method to refine our parameters.
        var refinedParameters = [CGFloat](count: range.length, repeatedValue: 0.0)
        for i in 0..<range.length {
            let fl = CGFloat((parameters[i] as NSNumber).doubleValue)
            refinedParameters[i] = NewtonsMethod(bezierPoints: bezierPoints, point: points[range.location + i], parameter: fl)
        }
        return refinedParameters;
    }

    func findMaximumErrorForBezierPoints(bezierPoints: BezierPoints, inRange range: NSRange, parameters: NSArray, maximumIndex: Int) -> FitCurveErrorInfo
    {
        var newMaximumIndex = maximumIndex

        // Here we calculate the squared errors, defined as:
        //
        //  S = SUM( (point[i] - Q(parameters[i])) ^ 2 )
        //
        //  Where point[i] is the point on this NSBezierPath at i, parameters[i] is the float in the parameters
        //  NSArray at index i. Q is the bezier curve represented by the variable bezier. This formula takes
        //  the difference (distance) between a point in this NSBezierPath we're trying to fit, and the corresponding
        //  point in the generated bezier curve, squares it, and adds all the differences up (i.e. squared errors).
        //  This tells us how far off our curve is from our points we're trying to fit.
        var maximumError: CGFloat = 0.0

        for i in 1..<(range.length - 1) {
            let pointOnQ = bezierPoints.pointAtRatio(parameters[i] as CGFloat) // Calculate Q(parameters[i])
            let point = points[range.location + i]
            let distance = FBPointSquaredLength(FBSubtractPoint(pointOnQ, point))
            if ( distance >= maximumError ) {
                maximumError = distance;
                newMaximumIndex = range.location + i
            }
        }

        return FitCurveErrorInfo(maximumError: maximumError, maximumIndex: newMaximumIndex)
    }

    func fitBezierInRange(range: NSRange, withParameters parameters: NSArray, leftTangent: CGPoint, rightTangent: CGPoint) -> BezierPoints
    {
        // We want to create a bezier curve to fit the path. A bezier curve is simply four points:
        //  the two end points, and a control point for each end point. We already have the end points
        //  (i.e. [self pointAtIndex:range.location] and [self pointAtIndex:range.location+range.length-1])
        //  and the direction of the control points away from the end points (leftTangent and rightTangent).
        //  The only thing lacking is the distance of the control points from their respective end points.
        //  This function computes those distances, called leftAlpha and rightAlpha, and then constructs
        //  the bezier curve from that.
        //
        // The basic formula used here for fitting a bezier is:
        //
        //  leftEndPoint = [self pointAtIndex:0]
        //  rightEndPoint = [self pointAtIndex:[self elementCount]-1]
        //  leftControlPoint = leftAlpha * leftTangent + leftEndPoint
        //  rightControlPoint = rightAlpha * rightTangent + rightEndPoint

        // By controlling the distance of the control points from their end points, we can affect the curve
        //  of the bezier. We want to chose distances (leftAlpha and rightAlpha) such that it best fits the
        //  points in self. Formally stated, we want to minimize the squared errors as represented by:
        //
        //  S = SUM( (point[i] - Q(parameters[i])) ^ 2 )
        //
        //  Where point[i] is the point on this NSBezierPath at i, parameters[i] is the float in the parameters
        //  NSArray at index i. Q is the bezier curve we're generating in this function. This formula takes
        //  the difference (distance) between a point in this NSBezierPath we're trying to fit, and the corresponding
        //  point in the generated bezier curve, squares it, and adds all the differences up (i.e. squared errors).
        //  This tells us how far off our curve is from our points we're trying to fit. We'd like to minimize this,
        //  when calculating leftAlpha and rightAlpha.
        //
        //  Using the least squares approach we can write the equations we want to solve:
        //
        //  d(S) / d(leftAlpha) = 0
        //  d(S) / d(rightAlpha) = 0
        //
        //  Here d() is the derivative, and S is the squared errors defined above. By then substituting in
        //  S and then Q(t), which is defined as:
        //
        //  Q(t) = SUM(V[i] * Bernstein[i](t))
        //
        //  Where i ranges between [0..3], V is one of the points on the bezier curve. V[0] is the leftEndPoint,
        //  V[1] the leftControlPoint, V[2] the rightControlPoint, and V[3] the rightEndPoint. Berstein[i] is
        //  is a polynomial defined by the Bernstein0(), Bernstein1(), Bernstein2(), Bernstein3() functions
        //  (see their comments for explaination).
        //
        // After much mathematical flagellation, we arrive at the following definitions
        //
        //  leftAlpha = det(X * C2) / det(C1 * C2)
        //  rightAlpha = det(C1 * X) / det(C1 * C2)
        //
        // Where:
        //
        //  C1 = [SUM(A1[i] * A1[i]), SUM(A1[i] * A2[i])]
        //  C2 = [SUM(A1[i] * A2[i]), SUM(A2[i] * A2[i])]
        //  X = [SUM(partOfX * A1[i]), SUM(partOfX * A2[i])]
        //      Where partOfX = (points[i] - (leftEndPoint * Bernstein0(parameters[i]) + leftEndPoint * Bernstein1(parameters[i]) + rightEndPoint * Bernstein2(parameters[i]) + rightEndPoint * Bernstein3(parameters[i])))
        //  A1[i] = leftTangent * Bernstein1(parameters[i])
        //  A2[i] = rightTangent * Bernstein2(parameters[i])

        // Create tables for the A values

        var a1 = [CGPoint](count: range.length, repeatedValue: CGPointZero)
        var a2 = [CGPoint](count: range.length, repeatedValue: CGPointZero)

        for i in 0..<range.length {
            a1[i] = FBUnitScalePoint(leftTangent, Bernstein1(parameters[i] as CGFloat));
            a2[i] = FBUnitScalePoint(rightTangent, Bernstein2(parameters[i] as CGFloat));
        }

        // Create the C1, C2, and X matrices

        var c1 = [CGFloat](count: 2, repeatedValue: 0.0)
        var c2 = [CGFloat](count: 2, repeatedValue: 0.0)
        var x = [CGFloat](count: 2, repeatedValue: 0.0)

        var partOfX = CGPointZero;
        var leftEndPoint = points[range.location]
        var rightEndPoint = points[range.location + range.length - 1]

        for i in 0..<range.length {
            c1[0] += FBDotMultiplyPoint(a1[i], a1[i]);
            c1[1] += FBDotMultiplyPoint(a1[i], a2[i]);
            c2[0] += FBDotMultiplyPoint(a1[i], a2[i]);
            c2[1] += FBDotMultiplyPoint(a2[i], a2[i]);

            partOfX = FBSubtractPoint(points[range.location + i],
                FBAddPoint(FBScalePoint(leftEndPoint, Bernstein0(parameters[i] as CGFloat)),
                    FBAddPoint(FBScalePoint(leftEndPoint, Bernstein1(parameters[i] as CGFloat)),
                        FBAddPoint(FBScalePoint(rightEndPoint, Bernstein2(parameters[i] as CGFloat)),
                            FBScalePoint(rightEndPoint, Bernstein3(parameters[i] as CGFloat))))));

            x[0] += FBDotMultiplyPoint(partOfX, a1[i]);
            x[1] += FBDotMultiplyPoint(partOfX, a2[i]);
        }

        // Calculate left and right alpha
        var c1AndC2: CGFloat = determinant(c1, c2);
        var xAndC2: CGFloat = determinant(x, c2);
        var c1AndX: CGFloat = determinant(c1, x);
        var leftAlpha: CGFloat = 0.0;
        var rightAlpha: CGFloat = 0.0;
        if ( c1AndC2 != 0.0 ) {
            leftAlpha = xAndC2 / c1AndC2;
            rightAlpha = c1AndX / c1AndC2;
        }

        // If the alpha values are too small or negative, things aren't going to work out well. Fall back
        //  to the simple heuristic
        var verySmallValue = 1.0e-6 * leftEndPoint.distanceToPoint(rightEndPoint)
        if ( leftAlpha < verySmallValue || rightAlpha < verySmallValue )
        {
            var leftTangentVector = CGVectorMake(leftTangent.x, leftTangent.y);
            var rightTangentVector = CGVectorMake(rightTangent.x, rightTangent.y);
            return self.fitBezierUsingNaiveMethodInRange(range, leftTangent:leftTangentVector, rightTangent:rightTangentVector)
        }

        // We already have the end points, so we just need the control points. Use alpha values
        //  to calculate those
        var leftControlPoint = FBAddPoint(FBUnitScalePoint(leftTangent, leftAlpha), leftEndPoint);
        var rightControlPoint = FBAddPoint(FBUnitScalePoint(rightTangent, rightAlpha), rightEndPoint);
        
        // Create the bezier path based on the end and control points we calculated
        return BezierPoints(points: [leftEndPoint, leftControlPoint, rightControlPoint, rightEndPoint])
    }


    func fitCubicToRange(range: NSRange, leftTangent leftTangentVector: CGVector, rightTangent rightTangentVector: CGVector, errorThreshold: CGFloat) -> BezierPath {
        let leftTangent = CGPointMake(leftTangentVector.dx, leftTangentVector.dy)
        let rightTangent = CGPointMake(rightTangentVector.dx, rightTangentVector.dy)

        // Handle the special case where we only have two points
        if ( range.length == 2 ) {
            let bezierPoints = fitBezierUsingNaiveMethodInRange(range, leftTangent:leftTangentVector, rightTangent:rightTangentVector)
            let path = BezierPath()
            path.appendSegmentWithPoints(bezierPoints, includeMoveTo: true)
            return path
        }

        // First thing, just try to fit one bezier curve to all our points in range
        var parameters = estimateParametersUsingChordLengthMethodInRange(range)
        var bezierPoints = fitBezierInRange(range, withParameters:parameters, leftTangent:leftTangent, rightTangent:rightTangent)

        // See how well our bezier fit our points. If it's within the allowed error, we're done
        var maximumIndex = NSNotFound

        var info = findMaximumErrorForBezierPoints(bezierPoints, inRange:range, parameters:parameters, maximumIndex:maximumIndex)
        var error = info.maximumError
        maximumIndex = Int(info.maximumIndex)

        if error < errorThreshold {
            let path = BezierPath()
            path.appendSegmentWithPoints(bezierPoints, includeMoveTo: true)
            return path
        }

        // Huh. That wasn't good enough. Well, our estimated parameters probably sucked, so see if it makes sense to try
        //  to refine them. If error is huge, it probably means that probably won't help, so in that case just skip it.
        if error < (errorThreshold * errorThreshold) {
            for i in 0..<fitCurveMaximumReparameterizes {
                parameters = refineParameters(parameters, forRange:range, bezierPoints:bezierPoints)

                // OK, try again with the new parameters
                bezierPoints = fitBezierInRange(range, withParameters:parameters, leftTangent:leftTangent, rightTangent:rightTangent)
                info = findMaximumErrorForBezierPoints(bezierPoints, inRange:range, parameters:parameters, maximumIndex:maximumIndex)
                error = info.maximumError
                maximumIndex = Int(info.maximumIndex)

                if error < errorThreshold {
                    let path = BezierPath()
                    path.appendSegmentWithPoints(bezierPoints, includeMoveTo: true)
                    return path // sweet, it worked!
                }
            }
        }

        // Alright, we couldn't fit a single bezier curve to all these points no matter how much we refined the parameters.
        //  Instead, split the points into two parts based on where the biggest error is. Build two separate curves which
        //  we'll combine into one single NSBezierPath.
        let centerTangent = computeCenterTangentAtIndex(maximumIndex)

        let leftBezier = fitCubicToRange(NSMakeRange(range.location, maximumIndex - range.location + 1), leftTangent:leftTangentVector, rightTangent:centerTangent, errorThreshold:errorThreshold)

        let rlTangent = CGVectorMake(-centerTangent.dx, -centerTangent.dy);
        
        let rightBezier = fitCubicToRange(NSMakeRange(maximumIndex, (range.location + range.length) - maximumIndex), leftTangent:rlTangent, rightTangent:rightTangentVector, errorThreshold:errorThreshold)

        rightBezier.enumerateSegments() {(type: BezierSegmentType, points: BezierPoints) in
            if type != .Move {
                leftBezier.appendSegmentWithPoints(points, includeMoveTo: false)
            }
        }

        return leftBezier
    }
}
