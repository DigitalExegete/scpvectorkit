//
//  BezierPath+Segments.swift
//  SCPVectorKit
//
//  Created by Semicolon on 10/26/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import Foundation
import CoreGraphics

public enum BezierSegmentType: Int {
    case Move = 0
    case Line = 1
    case Quad = 2
    case Cubic = 3
    case Close = 4
}

extension BezierPath {
    /**
    Enumerates over all elements in the path, passing in the current
    element information. Note that close elements include an endPoint
    that matches the subpath's start point.
    */
    public func enumerateSegments(block: (type: BezierSegmentType, points: BezierPoints) -> Void) {
        var subpathStartPoint = CGPointZero
        var previousPoint = CGPointZero


        SCPPathEnumerateUsingBlock(self.CGPath, { (cgElement: CGPathElement) -> Void in
            let cgElementPoints = cgElement.points

            var segmentType: BezierSegmentType!
            var points: BezierPoints!

            switch cgElement.type.value {
            case kCGPathElementMoveToPoint.value:
                segmentType = .Move
                points = BezierPoints(points: [previousPoint, cgElementPoints[0]])
                subpathStartPoint = cgElementPoints[0]

            case kCGPathElementAddLineToPoint.value:
                segmentType = .Line
                points = BezierPoints(points: [previousPoint, cgElementPoints[0]])

            case kCGPathElementAddQuadCurveToPoint.value:
                segmentType = .Quad
                points = BezierPoints(points: [previousPoint, cgElementPoints[0], cgElementPoints[1]])

            case kCGPathElementAddCurveToPoint.value:
                segmentType = .Cubic
                points = BezierPoints(points: [previousPoint, cgElementPoints[0], cgElementPoints[1], cgElementPoints[2]])

            case kCGPathElementCloseSubpath.value:
                segmentType = .Close
                points = BezierPoints(points: [previousPoint, subpathStartPoint])

            default:
                fatalError("invalid CGElementType \(cgElement.type) encountered in BezierPath.enumerateSegments")
            }

            block(type: segmentType, points: points)

            previousPoint = points.last
        });
    }

    public func appendSegmentWithPoints(points: BezierPoints, includeMoveTo: Bool = false) {

        if isEmpty || (includeMoveTo && self.currentPoint != points.first) {
            self.moveToPoint(points.first)
        }

        switch(points.count) {
        case 2:
            self.appendLineToPoint(points.last)

        case 3:
            self.appendQuadCurveWithControlPoint(points[1], toPoint:points[2])

        case 4:
            self.appendCubicCurveWithControlPoint1(points[1], controlPoint2: points[2], toPoint: points[3])

        default:
            fatalError("BezierPath.appendSegmentWithPoints called with invalid count of points: \(points.count)")
        }
    }

    public func pathByApplyingPointConverter(converter: (CGPoint) -> CGPoint) -> BezierPath {
        let path = BezierPath()

        self.enumerateSegments { (type: BezierSegmentType, points: BezierPoints) in

            var pointsCopy = points
            pointsCopy.applyPointConverter(converter)

            switch(type) {
            case .Move:
                path.moveToPoint(pointsCopy.first)

            case .Line, .Quad, .Cubic:
                path.appendSegmentWithPoints(pointsCopy)

            case .Close:
                path.closeSubpath()
            }
        }

        return path
    }

    public func flattenedPathWithFlatness(flatness: CGFloat) -> BezierPath {
        let newBezierPath = BezierPath()

        self.enumerateSegments { (type: BezierSegmentType, points: BezierPoints) in

            switch type {
            case .Move:
                newBezierPath.moveToPoint(points.last)

            case .Close:
                newBezierPath.closeSubpath()

            default:
                newBezierPath.appendFlattenedSegmentWithPoints(points, flatness: flatness)
            }
        }

        return newBezierPath
    }

    private func appendFlattenedSegmentWithPoints(points: BezierPoints, flatness: CGFloat) {

        switch points.count {

        case 2:
            self.appendSegmentWithPoints(points)

        case 3, 4:
            if points.flatness < flatness {
                self.appendLineToPoint(points.last)
            }
            else {
                let (minSegment, maxSegment) = points.splitAtRatio(0.5)

                self.appendFlattenedSegmentWithPoints(minSegment, flatness: flatness)
                self.appendFlattenedSegmentWithPoints(maxSegment, flatness: flatness)
            }

        default:
            fatalError("BezierPath.appendFlattenedSegmentWithPoints(flatness:) called with invalid count of points: \(points.count)")
        }
    }

    public func approximatelyEquals(otherPath: BezierPath) -> Bool {
        var theseTypes = [BezierSegmentType]()
        var thesePoints = [BezierPoints]()

        otherPath.enumerateSegments { (type, points) in
            theseTypes.append(type)
            thesePoints.append(points)
        }

        var i: Int = 0
        var equal = true
        
        self.enumerateSegments() { (type, points) in
            if theseTypes[i] != type {
                equal = false
            }
            
            if !thesePoints[i].approximatelyEquals(points) {
                equal = false
            }
            
            i++
        }
        
        return equal && (windingRule == otherPath.windingRule)
    }
    
}
