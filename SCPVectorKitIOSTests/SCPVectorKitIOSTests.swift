//
//  SCPVectorKitIOSTests.swift
//  SCPVectorKitIOSTests
//
//  Created by Semicolon on 9/10/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import UIKit
import XCTest
import SCPVectorKitIOS

class SCPVectorKitIOSTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

    func testPathEnumeration()
    {
        var path = CGPathCreateMutable()
        CGPathMoveToPoint(path, nil, 20.0, 40.0)
        CGPathAddLineToPoint(path, nil, 80.0, -120.0)
        CGPathAddCurveToPoint(path, nil, 25.0, -30.0, 42.0, 96.0, -17.5, -326.0)
        CGPathAddQuadCurveToPoint(path, nil, -4265.23, -96.34, 94.12, 23.45)
        CGPathCloseSubpath(path);
        CGPathMoveToPoint(path, nil, 20.0, 40.0)

        var index = 0

        SCPPathEnumerateUsingBlock(path, { (CGPathElement element) -> Void in

            if(index == 0 || index == 5)
            {
                XCTAssertEqual(element.type.value, kCGPathElementMoveToPoint.value, "moveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 20.0, y: 40.0), element.points[0], "moveTo enumeration failed")
            }
            else if(index == 1)
            {
                XCTAssertEqual(element.type.value, kCGPathElementAddLineToPoint.value, "lineTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 80.0, y: -120.0), element.points[0], "lineTo enumeration failed")
            }
            else if(index == 2)
            {
                XCTAssertEqual(element.type.value, kCGPathElementAddCurveToPoint.value, "curveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 25.0, y: -30.0), element.points[0], "curveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 42.0, y: 96.0), element.points[1], "curveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: -17.5, y: -326.0), element.points[2],"curveTo enumeration failed")
            }
            else if(index == 3)
            {
                XCTAssertEqual(element.type.value, kCGPathElementAddQuadCurveToPoint.value, "quadCurveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: -4265.23, y: -96.34), element.points[0], "quadCurveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 94.12, y: 23.45), element.points[1], "quadCurveTo enumeration failed")
            }
            else if(index == 4)
            {
                XCTAssertEqual(element.type.value, kCGPathElementCloseSubpath.value, "close enumeration failed")
            }

            index++
        })

        XCTAssertEqual(index, 6, "count of elements in enumeration failed")

        index = 0

        var wrappedPath = BezierPath(CGPath: path)
        wrappedPath.enumerateSegments { (type: BezierSegmentType, points: BezierPoints) -> Void in
            if(index == 0 || index == 5)
            {
                XCTAssertEqual(type, BezierSegmentType.Move, "wrapped moveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 20.0, y: 40.0), points.last, "wrapped moveTo enumeration failed")
            }
            else if(index == 1)
            {
                XCTAssertEqual(type, BezierSegmentType.Line, "wrapped lineTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 80.0, y: -120.0), points.last, "wrapped lineTo enumeration failed")
            }
            else if(index == 2)
            {
                XCTAssertEqual(type, BezierSegmentType.Cubic, "wrapped curveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 25.0, y: -30.0), points[1], "wrapped curveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 42.0, y: 96.0), points[2], "wrapped curveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: -17.5, y: -326.0), points.last, "wrapped curveTo enumeration failed")
            }
            else if(index == 3)
            {
                XCTAssertEqual(type, BezierSegmentType.Quad, "quadCurveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: -4265.23, y: -96.34), points[1], "quadCurveTo enumeration failed")
                XCTAssertEqual(CGPoint(x: 94.12, y: 23.45), points.last, "quadCurveTo enumeration failed")
            }
            else if(index == 4)
            {
                XCTAssertEqual(type, BezierSegmentType.Close, "close enumeration failed")
                XCTAssertEqual(CGPoint(x: 20.0, y: 40.0), points.last, "close enumeration failed")
            }

            index++
        }
    }

    func testPathCurveFitting() {

        let nbp = BezierPath()
        nbp.moveToPoint(CGPoint(x: 0.0, y: 0.0))
        nbp.appendLineToPoint(CGPoint(x: 5.0, y: 0.0))
        nbp.appendLineToPoint(CGPoint(x: 5.0, y: 5.0))
        nbp.appendLineToPoint(CGPoint(x: 20.0, y: 15.0))
        nbp.appendLineToPoint(CGPoint(x: 22.0, y: 28.0))
        nbp.appendLineToPoint(CGPoint(x: 90.0, y: 15.0))
        nbp.appendLineToPoint(CGPoint(x: 120.0, y: 80.0))

        let smoothed = nbp.smoothedPathWithTolerance(40.0);

        //TODO: comparing paths with == doesn't work for checking results that need a tolerance.
        //        Path <0x100256260>
        //        Bounds: {{0, 0}, {120, 80}}
        //        Control point bounds: {{0, -24.02932754176382}, {121.5066429473963, 104.02932754176382}}
        //        0.000000 0.000000 moveto
        //        3.247596 0.000000 18.353190 12.771963 20.000000 15.000000 curveto
        //        22.605998 18.525762 17.615685 28.000000 22.000000 28.000000 curveto
        //        121.506643 28.000000 71.986464 -24.029328 120.000000 80.000000 curveto
    }
}
