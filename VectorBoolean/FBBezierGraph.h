//
//  FBBezierGraph.h
//  VectorBoolean
//
//  Created by Andrew Finnell on 6/15/11.
//  Copyright 2011 Fortunate Bear, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

#if SCP_TARGET_OSX
#import <AppKit/AppKit.h>
#endif

@class FBBezierContour;
@class FBCurveLocation;

// FBBezierGraph is more or less an exploded version of an NSBezierPath, and
//  the two can be converted between easily. FBBezierGraph allows boolean
//  operations to be performed by allowing the curves to be annotated with
//  extra information such as where intersections happen.
@interface FBBezierGraph : NSObject {
    NSMutableArray *_contours;
    CGRect _bounds;
}

+ (instancetype) bezierGraph;

#if SCP_TARGET_OSX
+ (instancetype) bezierGraphWithBezierPath:(NSBezierPath *)path;
- (instancetype) initWithBezierPath:(NSBezierPath *)path;
- (NSBezierPath *) bezierPath;
#endif

+ (instancetype) bezierGraphWithCGPath:(CGPathRef)path;
- (instancetype)initWithCGPath:(CGPathRef)path;
- (CGMutablePathRef) newCGPath CF_RETURNS_RETAINED;

- (FBBezierGraph *) unionWithBezierGraph:(FBBezierGraph *)graph;
- (FBBezierGraph *) intersectWithBezierGraph:(FBBezierGraph *)graph;
- (FBBezierGraph *) differenceWithBezierGraph:(FBBezierGraph *)graph;
- (FBBezierGraph *) xorWithBezierGraph:(FBBezierGraph *)graph;


@property (readonly) NSArray* contours;
@property (readonly) CGRect bounds;

//defaults to NO;
@property BOOL usesNonZeroWindingRule;

- (FBCurveLocation *) closestLocationToPoint:(CGPoint)point;

@end
