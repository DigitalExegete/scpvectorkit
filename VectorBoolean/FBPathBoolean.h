//
//  FBPathBoolean.h
//  VectorBoolean
//
//  Created by Paul Sim on 8/29/14.
//  Copyright (c) 2014 Fortunate Bear, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

CGPathRef FBPathCreateWithUnionOfPaths(CGPathRef path1, CGPathRef path2) CF_RETURNS_RETAINED;
CGPathRef FBPathCreateWithIntersectOfPaths(CGPathRef path1, CGPathRef path2) CF_RETURNS_RETAINED;
CGPathRef FBPathCreateWithDifferenceOfPaths(CGPathRef path1, CGPathRef path2) CF_RETURNS_RETAINED;
CGPathRef FBPathCreateWithXorOfPaths(CGPathRef path1, CGPathRef path2) CF_RETURNS_RETAINED;

