//
//  BezierPath+Boolean.swift
//  SCPVectorKit
//
//  Created by Semicolon on 10/26/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import Foundation
import CoreGraphics

extension BezierPath {
    //TODO: add operators for boolean ops. Maybe @+, @-, @&, @^, respectively.

    //boolean ops code adapted from andy finnell,
    //https://bitbucket.org/andyfinnell/vectorboolean
    public func union(path: BezierPath) -> BezierPath {
        let union = FBPathCreateWithUnionOfPaths(self.CGPath, path.CGPath)
        return BezierPath(CGPath: union)
    }

    public func difference(path: BezierPath) -> BezierPath {
        let difference = FBPathCreateWithDifferenceOfPaths(self.CGPath, path.CGPath)
        return BezierPath(CGPath: difference)
    }

    public func intersect(path: BezierPath) -> BezierPath {
        let intersect = FBPathCreateWithIntersectOfPaths(self.CGPath, path.CGPath)
        return BezierPath(CGPath: intersect)
    }

    public func exclusiveOr(path: BezierPath) -> BezierPath {
        let xor = FBPathCreateWithXorOfPaths(self.CGPath, path.CGPath)
        return BezierPath(CGPath: xor)
    }
}