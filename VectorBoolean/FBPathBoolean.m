//
//  FBPathBoolean.m
//  VectorBoolean
//
//  Created by Paul Sim on 8/29/14.
//  Copyright (c) 2014 Fortunate Bear, LLC. All rights reserved.
//

#import "FBPathBoolean.h"
#import "FBBezierGraph.h"

CGPathRef FBPathCreateWithUnionOfPaths(CGPathRef path1, CGPathRef path2)
{
    FBBezierGraph *thisGraph = [FBBezierGraph bezierGraphWithCGPath:path1];
    FBBezierGraph *otherGraph = [FBBezierGraph bezierGraphWithCGPath:path2];
    CGPathRef result = [[thisGraph unionWithBezierGraph:otherGraph] newCGPath];
    return result;
}

CGPathRef FBPathCreateWithIntersectOfPaths(CGPathRef path1, CGPathRef path2)
{
    FBBezierGraph *thisGraph = [FBBezierGraph bezierGraphWithCGPath:path1];
    FBBezierGraph *otherGraph = [FBBezierGraph bezierGraphWithCGPath:path2];
    CGPathRef result = [[thisGraph intersectWithBezierGraph:otherGraph] newCGPath];
    return result;
}

CGPathRef FBPathCreateWithDifferenceOfPaths(CGPathRef path1, CGPathRef path2)
{
    FBBezierGraph *thisGraph = [FBBezierGraph bezierGraphWithCGPath:path1];
    FBBezierGraph *otherGraph = [FBBezierGraph bezierGraphWithCGPath:path2];
    CGPathRef result = [[thisGraph differenceWithBezierGraph:otherGraph] newCGPath];
    return result;
}

CGPathRef FBPathCreateWithXorOfPaths(CGPathRef path1, CGPathRef path2)
{
    FBBezierGraph *thisGraph = [FBBezierGraph bezierGraphWithCGPath:path1];
    FBBezierGraph *otherGraph = [FBBezierGraph bezierGraphWithCGPath:path2];
    CGPathRef result = [[thisGraph xorWithBezierGraph:otherGraph] newCGPath];
    return result;
}

