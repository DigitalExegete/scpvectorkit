//
//  Geometry.swift
//  SCPVectorKit
//
//  Created by Semicolon on 10/25/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

import Foundation

extension CGPoint {
    public func distanceToPoint(otherPoint: CGPoint) -> CGFloat {
        return hypot(x - otherPoint.x, y - otherPoint.y)
    }
}

extension CGVector {
    public mutating func normalize() {
        let d = hypot(dx, dy)
        dx /= d
        dy /= d
    }
}
