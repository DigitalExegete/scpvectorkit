//
//  SCPVectorKitOSX.h
//  SCPVectorKitOSX
//
//  Created by Semicolon on 10/23/14.
//  Copyright (c) 2014 Semicolon Productions. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SCPVectorKitOSX.
FOUNDATION_EXPORT double SCPVectorKitOSXVersionNumber;

//! Project version string for SCPVectorKitOSX.
FOUNDATION_EXPORT const unsigned char SCPVectorKitOSXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SCPVectorKitOSX/PublicHeader.h>

#import <SCPVectorKitOSX/SCPVectorUtilities.h>
#import <SCPVectorKitOSX/FBPathBoolean.h>
#import <SCPVectorKitOSX/MWCGPath.h>

#import "NSBezierPath+SCPVectorKit.h"
#import "NSBezierPath+Utilities.h"
#import "NSBezierPath+FitCurve.h"
